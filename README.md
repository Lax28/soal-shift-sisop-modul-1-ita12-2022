# soal-shift-sisop-modul-1-ITA12-2022
## Anggota Kelompok
1. Muhammad Jovan Adiwijaya Yanuarsyah (5027201025)
2. Made Gede Krisna Wangsa (5027201047)
3. Nida'ul Faizah (5027201064)

## Description
Laporan ini dibuat dengan tujuan untuk menjelaskan pengerjaan serta permasalahan yang kami alami dalam pengerjaan soal shift sistem operasi modul 1 tahun 2022.

## Soal Shift 1
Pada soal shift ini saya masih bingung untuk mengaplikasikan kondisi untuk pembuatan password. selain itu, saya masih belum mengetahui cara membuat kedua menu yang dibutuhkan untuk soal bagian d. Yang dapat saya lakukan hanyalah membuat bash script simple yang hanya menanyakan username dan password dengan output setelah mengisi akan ada di dalam user.txt (passwordnya hidden hanya saat diinput).

![image-1.png](./image-1.png)
![image-2.png](./image-2.png)

Seperti yang tertera pada gambar, saya dapat memindahkan output dari register.sh ke dalam user.txt serta membuat password dalam status "hidden".
## Soal Shift 2
pada soal shif 2 ini saya benar-benar bingung harus melakukan apa. Saat saya mencari referensi rata-rata membahas apakah server pada website sedang aktif atau tidak. Saat saya mencoba untuk mengetahui isi dari website pada soal yaitu https://daffa.info hasilnya adalah kode error "404". Jadi intinya, saya tidak dapat mencari log dari website tersebut.


## Soal Shift 3
pada soal shift 3 ini saya hanya bisa melakukan print dari free -m dan du -sh /home/lax/ (direktori saya) menggunakan bash script. Untuk pengecekan tiap menit saya coba menggunakan crontab tetapi tidak ada outputnya. oleh karena itu, saya juga tidak bisa membuat script kedua yaitu aggregate_minutes_to_hourly_log.sh.

![image-3.png](./image-3.png)

Gambar tersebut menunjukkan output dari script minute_log.sh.
